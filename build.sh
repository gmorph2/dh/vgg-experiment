#!/bin/sh

CONTEXT_ROOT=.
IMAGE_NAME=vic

docker build -t $IMAGE_NAME -f ${CONTEXT_ROOT}/Dockerfile ${CONTEXT_ROOT}
