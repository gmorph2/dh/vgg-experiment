#!/bin/sh

IMAGE=vic
CONTAINER=vic
HOST_SHARED_DIR=/opt/docker-shared/vgg
HOST_APPDATA=${HOST_SHARED_DIR}/vic
HOST_USERDATA=${HOST_SHARED_DIR}/mydata

echo "Stopping the running $CONTAINER container (if any)"
running=$(docker inspect -f {{.State.Running}} $CONTAINER 2> /dev/null)
if [ "${running}" = 'true' ]; then
  docker stop $CONTAINER
fi

echo "Removing the existing $CONTAINER container (if any)"
inactive_id=$(docker ps -aq -f status=exited -f status=created -f name=${CONTAINER})
if [ "${inactive_id}" != '' ]; then
  docker rm $CONTAINER
fi

set -x

docker run -d \
  --name $CONTAINER \
  -v ${HOST_APPDATA}:/appdata:Z \
  -v ${HOST_USERDATA}:/userdata:Z \
  -p 127.0.0.1:8000:8000 \
  $IMAGE
